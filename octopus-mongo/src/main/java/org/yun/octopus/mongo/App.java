package org.yun.octopus.mongo;

import com.alibaba.fastjson.JSON;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.yun.octopus.mongo.async.MongoAsyncMark;
import org.yun.octopus.mongo.core.MongoSyncMark;

import javax.annotation.Resource;

/**
 * @ProjectName: octopus
 * @ClassName: App
 * @Description: TODO(一句话描述该类的功能)
 * @Author: liyunfeng31
 * @Date: 2021/2/2 4:22
 */
@SpringBootApplication
public class App implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }


    @Resource
    private MongoSyncMark mark;


    @Resource
    private MongoAsyncMark asyncMark;

    @Override
    public void run(String... args) throws Exception {
        mark.save("trade","999",2,2);
        asyncMark.batchSave();
        System.out.println(JSON.toJSONString(1));
    }
}
