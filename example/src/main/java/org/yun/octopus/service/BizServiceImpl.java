package org.yun.octopus.service;

import org.springframework.aop.framework.AopContext;
import org.springframework.stereotype.Service;

import org.yun.octopus.base.annotation.Mark;
import org.yun.octopus.base.enums.RoleType;
import org.yun.octopus.rpc.RpcAApi;
import org.yun.octopus.rpc.RpcBApi;
import org.yun.octopus.rpc.RpcCApi;
import org.yun.octopus.rpc.dto.BizDto;

import javax.annotation.Resource;

/**
 * @author liyunfeng31
 */
@Service
public class BizServiceImpl implements BizService {

    @Resource
    private RpcAApi rpcA;

    @Resource
    private RpcBApi rpcB;

    @Resource
    private RpcCApi rpcC;


    @Override
    public int doBiz(BizDto dto) {
        rpcA.rpcA(dto);
        rpcB.rpcB(dto);
        rpcC.rpcC(dto);

        try {
            ((BizServiceImpl)AopContext.currentProxy()).save(dto);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 1;
    }


    @Mark(group = "createOrder",field = "orderId",stepNo = 4, rollback = "del", role = RoleType.END)
    public void save(BizDto dto) throws Exception {
        System.out.println("========== save local order ==========");
    }

    public void del(Long orderId){
        System.out.println("========== delete local order ==========");
    }
}
