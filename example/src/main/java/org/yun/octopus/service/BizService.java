package org.yun.octopus.service;


import org.yun.octopus.rpc.dto.BizDto;

public interface BizService{

   int doBiz(BizDto dto);
}
