package org.yun.octopus.mq;

import org.springframework.stereotype.Service;
import org.yun.octopus.base.spi.IMqService;

import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;


/**
 * @author liyunfeng31
 */
@Service
public class MqServiceImpl implements IMqService {

    @Override
    public void sendFallbackMsg(String group, String bizNo, List<Integer> targetNos) {
        System.out.println(" MQ  send msg, bizNo: "+bizNo);
    }
}
