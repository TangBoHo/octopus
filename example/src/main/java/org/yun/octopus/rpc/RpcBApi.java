package org.yun.octopus.rpc;

import org.yun.octopus.rpc.dto.BizDto;

public interface RpcBApi {

    Object rpcB(BizDto dto);
}
