package org.yun.octopus.rpc.dto;

/**
 * @author liyunfeng31
 */
public class RpcResp {

    private int code;

    private Object data;

    private String eMsg;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String geteMsg() {
        return eMsg;
    }

    public void seteMsg(String eMsg) {
        this.eMsg = eMsg;
    }

    public RpcResp(int code, Object data) {
        this.code = code;
        this.data = data;
    }
}
