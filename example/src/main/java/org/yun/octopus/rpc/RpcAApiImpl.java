package org.yun.octopus.rpc;

import org.springframework.stereotype.Service;
import org.yun.octopus.base.annotation.Mark;
import org.yun.octopus.rpc.dto.BizDto;
import org.yun.octopus.rpc.dto.RpcResp;

/**
 * @author liyunfeng31
 */
@Service
public class RpcAApiImpl implements RpcAApi{

    @Mark(group = "createOrder",field = "orderId", stepNo = 1, rollback = "rollbackAAA")
    @Override
    public Object rpcA(BizDto dto) {
        System.out.println("执行 [ A ] 的正向方法了");
        return new RpcResp(1,111);
    }

    public Object rollbackAAA(Long id) {
        System.out.println("执行 [ A ] 的回滚方法了");
        return new RpcResp(1,111);
    }
}
