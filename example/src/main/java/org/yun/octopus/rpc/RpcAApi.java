package org.yun.octopus.rpc;


import org.yun.octopus.rpc.dto.BizDto;

public interface RpcAApi {

    Object rpcA(BizDto dto);
}
