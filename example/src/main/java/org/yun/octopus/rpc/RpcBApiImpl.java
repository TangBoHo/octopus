package org.yun.octopus.rpc;

import org.springframework.stereotype.Service;
import org.yun.octopus.base.annotation.Mark;
import org.yun.octopus.rpc.dto.BizDto;
import org.yun.octopus.rpc.dto.RpcResp;

/**
 * @author liyunfeng31
 */
@Service
public class RpcBApiImpl implements RpcBApi{


    @Mark(group = "createOrder",field = "orderId",stepNo = 2, rollback = "rollbackBBB")
    @Override
    public Object rpcB(BizDto dto) {
        System.out.println("执行 [ B ] 的正向方法了");
        return new RpcResp(1,222);
    }


    public Object rollbackBBB(Long id) {
        System.out.println("执行 [ B ] 的回滚方法了");
        return new RpcResp(1,222);
    }
}
