package org.yun.octopus.rpc.dto;

public class BizDto {

    private Long orderId;

    private int num;

    public BizDto(Long orderId, int num) {
        this.orderId = orderId;
        this.num = num;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    @Override
    public String toString() {
        return "RpcADto{" +
                "orderId=" + orderId +
                ", num=" + num +
                '}';
    }
}
