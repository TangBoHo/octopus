package org.yun.octopus.rpc;

import org.springframework.stereotype.Service;
import org.yun.octopus.base.annotation.Mark;
import org.yun.octopus.rpc.dto.BizDto;
import org.yun.octopus.rpc.dto.RpcResp;


/**
 * @author liyunfeng31
 */
@Service
public class RpcCApiImpl implements RpcCApi{


    @Mark(group = "createOrder",field = "orderId",stepNo = 3, rollback = "rollbackCCC")
    @Override
    public Object rpcC(BizDto dto) {
        System.out.println("执行 [ C ] 的正向方法了");
       /* if(1 == 1){
           return new TimeOutException();
        }*/
        return new RpcResp(1,333);
    }

    public Object rollbackCCC(Long id) {
        System.out.println("执行 [ C ] 的回滚方法了");
        return new RpcResp(1,222);
    }
}
