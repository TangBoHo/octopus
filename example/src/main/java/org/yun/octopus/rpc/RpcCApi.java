package org.yun.octopus.rpc;

import org.yun.octopus.rpc.dto.BizDto;

public interface RpcCApi {

    Object rpcC(BizDto dto);
}
