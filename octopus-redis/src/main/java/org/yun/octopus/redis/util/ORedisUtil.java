package org.yun.octopus.redis.util;


import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;


import java.util.Map;
import java.util.Set;


/**
 * @author liyunfeng31
 */
@Component
public class ORedisUtil {

    @Resource
    private StringRedisTemplate template;



    public void del(String k){
        template.delete(k);
    }

    public Object hGet(String k, String hk){
        return template.opsForHash().get(k, hk);
    }


    public void hPut(String k, Object hk, Object hv){
        template.opsForHash().put(k, hk, hv);
    }


    public void hPutAll(String k, Map map){
        template.opsForHash().putAll(k, map);
    }


    public Map<Object, Object> hGetAll(String k){
        return template.opsForHash().entries(k);
    }


    public Boolean zAdd(String k, String m, double score){
        return template.opsForZSet().add(k, m, score);
    }


    public void zRemoveMember(String k, String m){
        template.opsForZSet().remove(k, m);
    }


    public Set<String> zRangeByScore(String k, double var2, double var4, long var6, long var8){
       return template.opsForZSet().rangeByScore(k, var2, var4, var6, var8);
    }


}
