# ***OCTOPUS***
   
------
### [项目简介](https://gitee.com/TangBoHo/octopus)

> OCTOPUS是一种基于缓存打标的分布式事务处理框架。 
通过AOP对外部调用，内部操作等流程节点的结果打标，监控流程并作为异常回滚的依据。
使之业务只需要配置好逆向接口以后，只需关注业务流程，如rpcAService->rpcBService->saveDB->rpcCService
而无需关心事务，回滚，数据一致性等问题。>

######特点 

> * 面向接口，而非数据，依据业务流程id
> * 业务发起者又是协调者，减少了组件
> * 使用缓存redis/mongo，性能高
> * 支持同步回滚和异步回滚，参与者自己实现幂等
> * 可信模式和不可信模式，可信模式只回滚确定的资源，性能低，不可信模式性能高，异常全回滚


### 架构设计
![binaryTree](架构.png)

### [使用说明](https://gitee.com/TangBoHo/octopus)

> * 项目引入jar
> * 选择缓存组件,redis or mongo
> * 配置注解和逆向方法


### [更新目标](https://gitee.com/TangBoHo/octopus)

> * 识别不统一的rpc响应体
> * 支持正向重试

