package org.yun.octopus.base.enums;

/**
 * @author liyunfeng31
 */

public enum ResType {
    /**
     * ResultType
     */
    SUCCESS(1),
    FAIL(0),
    UNKNOWN(2),
    FALLBACK_SUCCESS(3);

    private int code;

    ResType(int code) {
        this.code = code;
    }

    public int code() {
        return code;
    }
}
