package org.yun.octopus.base.enums;

/**
 * @author liyunfeng31
 */

public enum CallType {

    /**
     * Mode
     * 1-同步调用且部分step
     * 2-异步消费MQ且部分step
     * 3-(MQ/脚本补偿)同步调用全部step
     */
    SYNC_PART(1),
    MQ_PART(2),
    SYNC_ALL(3);

    private int code;

    CallType(int code) {
        this.code = code;
    }

    public int code() {
        return code;
    }
}
