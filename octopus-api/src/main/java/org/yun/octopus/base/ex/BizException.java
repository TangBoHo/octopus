package org.yun.octopus.base.ex;

/**
 * @author liyunfeng31
 */
public class BizException extends Exception{

    private int code;

    private String message;

    public BizException() {
    }

    public BizException(String message) {
        super(message);
        this.message = message;
    }

    public BizException(int code, String message) {
        super(message);
        this.code = code;
        this.message = message;
    }


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
