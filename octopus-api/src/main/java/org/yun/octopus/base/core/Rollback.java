package org.yun.octopus.base.core;

import org.yun.octopus.base.enums.RoleType;

import java.lang.reflect.Method;

/**
 * @author liyunfeng31
 */
public class Rollback {

    private Object clz;


    private Method method;


    private RoleType role;


    public Rollback(Object clz, Method method, RoleType role) {
        this.clz = clz;
        this.method = method;
        this.role = role;
    }

    public Object getClz() {
        return clz;
    }

    public void setClz(Object clz) {
        this.clz = clz;
    }

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    public RoleType getRole() {
        return role;
    }

    public void setRole(RoleType role) {
        this.role = role;
    }
}
