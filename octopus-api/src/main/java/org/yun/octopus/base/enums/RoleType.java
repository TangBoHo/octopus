package org.yun.octopus.base.enums;

/**
 * @author liyunfeng31
 */

public enum RoleType {

    /**
     * 角色类型
     */
    NORMAL(0),
    PARENT(1),
    CHILD(2),
    END(3);

    private int code;

    RoleType(int code) {
        this.code = code;
    }

    public int code() {
        return code;
    }
}
