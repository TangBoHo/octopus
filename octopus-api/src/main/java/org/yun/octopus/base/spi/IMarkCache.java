package org.yun.octopus.base.spi;


import java.util.List;

/**
 * @author liyunfeng31
 */
public interface IMarkCache {


    /**
     * 缓存打标
     * @param group 业务tag
     * @param bizNo 业务编号
     * @return result
     */
    boolean syncSave(String group, String bizNo);

    /**
     * 缓存打标
     * @param group 业务tag
     * @param bizNo 业务编号
     * @param stepNo 步骤编号
     * @param code 步骤结果标记
     */
    boolean save(String group, String bizNo, int stepNo, int code);


    /**
     * 缓存打标
     * @param group 业务tag
     * @param bizNo 业务编号
     * @param stepNo 步骤编号
     * @param code 步骤结果标记
     * @param childBizNo 子步骤业务编号
     * @return save result
     */
    boolean save(String group, String bizNo, int stepNo, int code, String childBizNo);


    /** 获取已存的目标编号
     * @param group group
     * @param bizNo 业务编号
     * @return list
     */
    List<Integer> stepNos(String group, String bizNo);


    /**
     * 清除mark标记
     * @param group 业务组
     * @param bizNo 业务编号
     * @return row
     */
    boolean clearMark(String group, String bizNo);


    /** 获取不正常完结的业务编号
     * @param group 业务组
     * @param st 开始时间
     * @param et 结束时间
     * @param limit 数据条数limit
     * @return bizNos
     */
    List<Object> abnormalNos(String group, long st, long et, int limit);


    /**
     * @param group 业务组
     * @param bizNo 业务编号
     * @param stepNo 步骤编号
     * @return 子业务ID
     */
    String childBizNo(String group, String bizNo, int stepNo);
}
