package org.yun.octopus.base.spi;

/**
 * @author liyunfeng31
 */
public interface ICorrect {


    /** 找到异常数据并处理
     * @param start 开始时间
     * @param end 结束时间
     * @param group 业务组
     * @param shardingNo 分组编号
     */
    void correctExData(long start, long end, String group, int shardingNo);
}
