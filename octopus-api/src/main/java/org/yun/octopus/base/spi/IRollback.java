package org.yun.octopus.base.spi;

/**
 * @author liyunfeng31
 */
public interface IRollback {

    /**
     * 逆向处理类
     * @param group 业务组
     * @param bizNo 业务编号
     * @param mode  1-调用且部分step  2-消费MQ且部分step   3-同步回滚全部step
     */
    void execute(String group, String bizNo, int mode) throws Exception;
}
