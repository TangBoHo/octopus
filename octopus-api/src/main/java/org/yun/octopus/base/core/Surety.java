package org.yun.octopus.base.core;

import org.springframework.stereotype.Component;
import org.yun.octopus.base.spi.IMarkCache;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;


/**
 * @author liyunfeng31
 */
@Component
public class Surety {

    private static IMarkCache cache;

    @Resource
    private IMarkCache markCache;

    @PostConstruct
    public void init() {
        cache = markCache;
    }

    /**
     * sync mark
     * @param group group
     * @param bizNo bizNo
     */
    public  static void ensure(String group, String bizNo){
        cache.syncSave(group,bizNo);
    }


}
