package org.yun.octopus.base.annotation;


import org.yun.octopus.base.enums.RoleType;

import java.lang.annotation.*;

import static org.yun.octopus.base.enums.RoleType.NORMAL;

/**
 * @author liyunfeng31
 */
@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Mark {

    /**
     * 业务组
     */
    String group() default "";


    /**
     * 主键字段
     */
    String field() default "";

    /**
     * 步骤编号
     */
    int stepNo() default 0;

    /**
     * 回滚方法
     */
    String rollback() default "";

    /**
     * 角色
     */
    RoleType role() default NORMAL;

}
