package org.yun.octopus.base.spi;


import java.util.List;

/**
 * @author liyunfeng31
 */
public interface IMqService {

    /**
     * 发送mq
     * @param group 业务组
     * @param bizNo 业务ID
     * @param stepNos 步骤编号
     */
    void sendFallbackMsg(String group, String bizNo, List<Integer> stepNos);
}
