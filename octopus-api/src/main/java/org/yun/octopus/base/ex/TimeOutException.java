package org.yun.octopus.base.ex;

/**
 * @author liyunfeng31
 */
public class TimeOutException extends Exception {

    private String message;

    public TimeOutException() {
    }

    public TimeOutException(String message) {
        super(message);
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
