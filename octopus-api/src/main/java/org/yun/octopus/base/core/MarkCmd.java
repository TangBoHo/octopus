package org.yun.octopus.base.core;

/**
 * @ProjectName: octopus
 * @ClassName: Cmd
 * @Description: mark封装类
 * @Author: liyunfeng31
 * @Date: 2021/1/30 12:40
 */
public class MarkCmd {

    private String group;

    private String bizNo;

    private int stepNo;

    private int code;

    private String childNo;


    public MarkCmd() {
    }

    public MarkCmd(String group, String bizNo) {
        this.group = group;
        this.bizNo = bizNo;
    }



    public MarkCmd(String group, String bizNo, int stepNo,int code, String childNo) {
        this.group = group;
        this.bizNo = bizNo;
        this.stepNo = stepNo;
        this.code = code;
        this.childNo = childNo;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getBizNo() {
        return bizNo;
    }

    public void setBizNo(String bizNo) {
        this.bizNo = bizNo;
    }

    public int getStepNo() {
        return stepNo;
    }

    public void setStepNo(int stepNo) {
        this.stepNo = stepNo;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getChildNo() {
        return childNo;
    }

    public void setChildNo(String childNo) {
        this.childNo = childNo;
    }
}
