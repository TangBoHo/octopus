package org.yun.octopus.base.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author liyunfeng31
 */
@Component
public class Cfg {

    /**
     * 打标是否异步
     */
    public static volatile boolean markAsync;

    /**
     * 回滚是否异步
     */
    public static boolean backAsync;

    /**
     * 返回值内判断结果的field 默认code
     */
    public static String codeKey;

    /**
     * 停顿时长 该时间内未完结的业务 视为异常终止
     */
    public static int suspend;

    /**
     * 扫描时间区间
     */
    public static int interval;


    @Value("${octopus.mark.markAsync:true}")
    public void setMarkAsync(boolean markAsync) {
        System.out.println("Cfg ### .markAsync-->  "+markAsync);
        Cfg.markAsync = markAsync;
    }

    @Value("${octopus.mark.backAsync:false}")
    public void setbackAsync(boolean backAsync) {
        Cfg.backAsync = backAsync;
    }


    @Value("${octopus.callback.codeKey:code}")
    public void setCodeKey(String codeKey) {
        Cfg.codeKey = codeKey;
    }


    @Value("${octopus.fail.suspend:10}")
    public void setSuspend(int suspend) {
        Cfg.suspend = suspend;
    }


    @Value("${octopus.fail.interval:60}")
    public void setInterval(int interval) {
        Cfg.interval = interval;
    }

}
