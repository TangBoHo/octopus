package org.yun.octopus.base.util;



import org.yun.octopus.base.enums.ResType;
import org.yun.octopus.base.ex.TimeOutException;

import static org.yun.octopus.base.config.Cfg.codeKey;
import static org.yun.octopus.base.enums.ResType.*;
import static org.yun.octopus.base.util.CommonUtil.valFromObj;

/**
 * @ProjectName: octopus
 * @ClassName: ParamHelper
 * @Description: 入参反参解析类，用于覆写
 * @Author: liyunfeng31
 * @Date: 2021/2/10 6:24
 */
public abstract class ParamHelper {


    public static String getBizNo(Object[] args){
        return args[0].toString();
    }

    /*public static String getBizNo2(Object[] args,String field){
        return valFromObj(args[0], field).asText();
    }*/


    /**
     * 解析结果
     * @param resp 返回结果
     * @return ResultType
     */
    public static ResType getResType(Object resp){

        if(resp == null){ return SUCCESS; }

        if(resp instanceof Exception){
            return resp instanceof TimeOutException ? UNKNOWN : FAIL;
        }
        int code = ParamHelper.getResCode(resp, codeKey);
        return SUCCESS.code() == code? SUCCESS : FAIL;
    }


    private static int getResCode(Object result, String field){
        return valFromObj(result,field).asInt();
    }

}
